#!/usr/bin/python
import yaml
import sys
from utils import *
from errors import *
from ansible_vault import Vault

def configYaml(args, filename='config.yaml'):

    v = ExtendedVault(args.v) # TODO accept relative paths
    try:
        configuration = yaml.safe_load(open(filename,'r'))
    except yaml.YAMLError as exc:
        print("Error reading configuration file ", exc)
    return configuration

def getConnDetails(args,configYaml):
    """
    getConnections

    This returns a dictionary within servernames and the connstrings.
    Do not use this to connect, use Server class to open a cursor and execute
    """
    d = {}

    vaultfile = getVaultFile(configYaml)

    if (args.v is None or args.v == "" ) and vaultfile is not None:
        vaultp = getVaultP(vaultfile)
    else:
        vaultp = getVaultP(args.v)

    v = ExtendedVault(vaultp)
    for server in configYaml['connections']:
        for opts in configYaml['connections'][server]:
            # parse here ansible vault when password
            passw = ""
            if opts == "password":
                vaultStr=configYaml['connections'][server][opts]
                if vaultStr.startswith('$ANSIBLE_VAULT;'):
                    try:
                        passw = v.load_raw(vaultStr)
                        passw = passw.decode('utf-8')
                    except Exception as e:
                        print(hErrMessage('VAULT_FAILED_DECRYPT'),e)
                        sys.exit(hErrNum('VAULT_FAILED_DECRYPT'))
                else:
                    passw = vaultStr #configYaml['connections'][server][opts]
                configYaml['connections'][server][opts] = passw
            #connstring += "{0} = '{1}' ".format(opts, configYaml['connections'][server][opts])
        d[server]=configYaml['connections'][server]
        if (configYaml['connections'][server]).get("user") is None:
            d[server]["user"] = "postgres" # default
        if (configYaml['connections'][server]).get("dbname") is None:
            d[server]["dbname"] = configYaml['connections'][server]["user"] # default
        if (configYaml['connections'][server]).get("port") is None:
            d[server]["port"] = "5432" # default  

    return d

def getConnections(args,configYaml):
    """
    getConnections

    This returns a dictionary within servernames and the connstrings.
    Do not use this to connect, use Server class to open a cursor and execute
    """
    d = {}
    vaultfile = getVaultFile(configYaml)

    if (args.v is None or args.v == "" ) and vaultfile is not None:
        vaultp = getVaultP(vaultfile)
    else:
        vaultp = getVaultP(args.v)

    v = ExtendedVault(vaultp)
    for server in configYaml['connections']:
        connstring = ""
        for opts in configYaml['connections'][server]:
            # parse here ansible vault when password
            passw = ""
            if opts == "password":
                vaultStr=configYaml['connections'][server][opts]
                if vaultStr.startswith('$ANSIBLE_VAULT;'):
                    try:
                        passw = v.load_raw(vaultStr)
                        passw = passw.decode('utf-8')
                    except Exception as e:
                        print(hErrMessage('VAULT_FAILED_DECRYPT'),e)
                        sys.exit(hErrNum('VAULT_FAILED_DECRYPT'))
                else:
                    passw = vaultStr #configYaml['connections'][server][opts]
                connstring += "{0} = '{1}' ".format(opts, passw)
            else:
                connstring += "{0} = '{1}' ".format(opts, configYaml['connections'][server][opts])
        d[server]=connstring
    return d

def getVaultFile(configYaml):
    if configYaml['vaultfile'] is None:
        return None
    else:
        return configYaml['vaultfile']

def getPublications(configYaml):
    if configYaml['publications'] is None:
        return None
    else:
        return configYaml['publications']

def getSubscriptions(configYaml):
    if configYaml['subscriptions'] is None:
        return None
    else:
        return configYaml['subscriptions']