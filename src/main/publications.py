#!/usr/bin/python

# import psycopg2
import utils

class Publication:
    """
    Publication handles both super-pub (pubName) and its suffixes
    (publishs are unique across all the publication)
    """
    
    _registry = []
    """
    Iterable instances of classes
    for p in Server._registry:
        print p
    """

    def __init__(self,pubName,yamltokens):
        self._pubName = pubName
        if yamltokens.get('tables') is None or 'all' in yamltokens['tables']:
            self._tables = "all"
        else:
            self._tables = ",".join(yamltokens['tables'])

        if  yamltokens.get('publish') is None or 'full'  in yamltokens['publish']:
            self._publish = 'insert,update,delete'
        else:
            self._publish = ",".join(yamltokens['publish'])
        self._connections = yamltokens['connections']

        if yamltokens.get('config') is None:
            pass
        else:
            # set here the configuration
            # - mode forced (it alters the tables to get the maximum publishing)
            # - _corner case_ publish with full repid on certain rule cehck 
            # (table size in cols and size, write rate and indexes)
            # - not_create that is, we assume it exists and then we store it fail or ok state
            pass
        
        self._registry.append(self)
        self.name = pubName
    @property
    def connections(self):
        return self._connections
    @property
    def tables(self):
        return self._tables
    @property
    def publish(self):
        return self._publish
       
    def getPubConnections(self):
        return self.connections

    def getPubPublish(self):
        return self.publish

    def getPubTables(self):
        sql = """
        SELECT pg_get_publication_tables('{0}')
        """.format(self._pubName)
        return sql
    
    def getPubStatus(self,slotname):
        """
        getPuStatus
        requires slotname, as it returns the status of the
        socket. sub.slotName() returns this
        """
        sql = """
        SELECT 
            slot_name   ,
            plugin              ,
            slot_type           ,
            datoid              ,
            database            ,
            temporary           ,
            active              ,
            active_pid          ,
            xmin                ,
            catalog_xmin        ,
            restart_lsn         ,
            confirmed_flush_lsn 
        FROM pg_replication_slots
        WHERE slot_name = '{0}'
        """.format(slotname)
        
        return sql
    def getLastValueSeq(self,table,schema='public'):
        """

        get the sequence value
        """
        sql = utils.Util().getLastValueSeqSQL(table,schema)
        return sql

    def getPubSeqname(self):
        sql = utils.Util().getPubSequences(self._pubName)
        return sql # returns relid, lvt
        

    def getAllPubLastValSeq(self):
        """
        getAllPubLastValSeq
        scans all tables in publication and return their sequences (identities
        is managed separately).
        """
        sql = utils.Util().getAllPubLastValSeqSQL(self._pubName)
        return sql # returns relid, lvt

    # Actions against Publications
    def getCreatePubSQL(self):
        # ,pubName,tables,publish
        if self.tables == "all":
            sql = """
            CREATE PUBLICATION {0} FOR ALL TABLES 
            WITH ( publish = '{1}')
            """.format(self._pubName,self._publish)
            #return #self.connection.execute(sqlCreatePub)
        else:
            sql = """
            CREATE PUBLICATION {0} FOR TABLE {1} 
            WITH ( publish = '{2}')
            """.format(self._pubName,self.tables,self._publish)
            #return self.connection.execute(sqlCreatePubAll)
        return sql 
    def addTable(self):
        if self.tables == "all":
            print("Supporting this only when per-table is enabled.")
        return True
    def dropTable(self):
        return True

    def getDropPubSQL(self):
        sql = """
        DROP PUBLICATION IF EXISTS {0}; 
        """.format(self._pubName)
        return sql
    ## Identities Handling
    def getLastValueIdentity(self):

        return True

    def setValueIdentity(self):
        # Theorically this is not needed, identity is inherited in table structure
        #ALTER TABLE {0} ALTER COLUMN {1} RESTART WITH {2};
        return True

