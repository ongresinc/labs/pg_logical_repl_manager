#!/usr/bin/python

import psycopg2
import sys 
# from installFunctions import sql_function_creation
import utils
from utils import *
from errors import *


class Server:
    """
    Class Server

    It holds a cursor for each instance.
    """

    _registry = []
    """
    Iterable instances of classes
    for p in Server._registry:
        print p
    """

    def __init__(self,server,fields,conn,min_supported_version,install):
        try:
            self._conn = psycopg2.connect(conn)
            self._cursor = self._conn.cursor()
        except Exception as e:
            sys.exit(utils.hErrMessage('FATAL_CONN_PG',server + " " + str(e)))
        if install is True:
            self.installFuncs()
        self._fields = fields 
        self.checkVersion(min_supported_version)
        self._registry.append(self)
        self.name = server
    @property
    def connection(self):
        return self._conn
    @property
    def cursor(self):
        return self._cursor
    #@property
    def fields(self):
        return self._fields

    def execsql(self, sql, params=None):
        self.cursor.execute(sql, params or())
        self.commit()
    def commit(self):
        return self.connection.commit()
    # def installFuncs(self):
    #     """
    #     installFuncs
    #     This function installs functions in installFunctions.py,
    #     TODO: Check that funcs are installed
    #     """
    #     print("Installing functions...")
    #     try:
    #         self.cursor.execute(sql_function_creation)
    #         self.connection.commit()
    #     except Exception as e:
    #         print("Error installing funcs: ", e)
    #         sys.exit(4)
    #     return True
    def checkVersion(self, min_supported_version):
        sql = utils.Util().versionIsSupported(min_supported_version)
        try:
            self.cursor.execute(sql)
            res = self.cursor.fetchone()
        except Exception as e:
            sys.exit(hErrMessage('FAIL_READ_CONN',e))
        self.commit()
        if res[0] == False:
            print("Version is not supported [yet?], minimal supported version: ", min_supported_version)
            sys.exit(3)

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def __exit__(self, exc_type, exc_val, exc_tb):
        # self.connection.commit()
        self.cursor.close()


def testConnections(connections,debug=False):
    for server in connections:
        try:
            conn = psycopg2.connect(connections[server])
            cursor = conn.cursor()
            cursor.execute('SELECT version();')
            print("Successfull conn: " + cursor.fetchone()[0])
        except Exception as e:
            if debug:
                print(connections[server])
            sys.exit(hErrMessage('TEST_CONN_FAIL',e))
    conn.close()
    return True
