#!/usr/bin/python
import configYaml
import sys
import utils
from utils import *
from errors import *

class Subscription:
    """
    Subscription

    This class manages all the SQL handling of the subscriptions
    Defaults follow the standard unless specified otherwise (some cases
    for practical purposes)
    https://www.postgresql.org/docs/10/static/sql-createsubscription.html
    """
    _registry = []
    """
    Iterable instances of classes
    for p in Server._registry:
        print p
    """

    def __init__(self,subName,yamltokens):
        self._subName = subName
        self._connections = yamltokens['connections']

        self._publications = yamltokens['publications']

        try:    
            if yamltokens.get('copy_data') is None or yamltokens.get('copy_data') == "":
                self._copy_data = "true"
            else:
                self._copy_data = utils.checkBooleanValues(yamltokens['copy_data'])

            if yamltokens.get('create_slot') is None or yamltokens.get('create_slot') == "":
                self._create_slot = "true"
            else:
                self._create_slot = utils.checkBooleanValues(yamltokens['create_slot'])

            if yamltokens.get('enabled') is None or yamltokens.get('enabled') == "":
                self._enabled = "true"
            else:
                self._enabled = utils.checkBooleanValues(yamltokens['enabled'])

            if yamltokens.get('connect') is None or yamltokens.get('connect') == "":
                self._connect = "true"
            else:
                self._connect = utils.checkBooleanValues(yamltokens['connect'])

            if yamltokens.get('slot_name') is None or yamltokens.get('slot_name') == "":
                self._slot_name = ""
                chars = set("""$/.,><[]{}|=-+)(*&^%$#@!~""")
                if self._subName.islower() is False and any(self._subName in chars): # XXX: 
                    sys.exit(hErrMessage('INVALID_CHARS_IN_SUBNAME')) 
            else:
                self._slot_name = yamltokens['slot_name']

            if yamltokens.get('synchronous_commit') is None or yamltokens.get('synchronous_commit') == "":
                self._synchronous_commit = "off"
            else:
                self._synchronous_commit = yamltokens['synchronous_commit']                
        except Exception as e:
            sys.exit(hErrMessage('YAML_CONF_SUBS_ERROR', str(e)))
               
        self._registry.append(self)
        self.name = subName

    # @property
    # def connections(self):
    #     return self._connections
    @property
    def publications(self):
        return self._publications
    @property
    def connections(self):
        return self._connections
    @property
    def slotName(self):
        return self._slot_name

    def getSubConns(self):
        return self.connections

    def getPauseSubSQL(self):
        sql = """
        ALTER SUBSCRIPTION {0} DISABLE
        """.format(self._subName)
        return sql
    
    def getResumeSubSQL(self):
        sql = """
        ALTER SUBSCRIPTION {0} ENABLE
        """.format(self._subName)
        return sql

    def refreshSubPub(self,copy_data='true'):
        sql = """
        ALTER SUBSCRIPTION {0} REFRESH PUBLICATION 
        WITH (refresh_option = {1})
        """.format(self._subName,copy_data)
        # copy_data is true by default
        return sql 
    def renameSub(self, former):
        sql = """
        ALTER SUBSCRIPTION {0} RENAME TO {1}
        """.format(former, self._subName)
        return sql 

    def getSubStatus(self):
        sql = """
        SELECT subid,subname,relid,received_lsn,
            last_msg_send_time,last_msg_receipt_time,
            latest_end_lsn,latest_end_time
        FROM pg_stat_subscription
        WHERE subname = '{0}'
        """.format(self._subName)
        return sql

    def getCreateSubSQL(self,conn):
        sql = """
        CREATE SUBSCRIPTION {0}
        CONNECTION '{1}'
        PUBLICATION {2}
        WITH 
        ( 
            copy_data = {3},
            create_slot = {4},
            {5}
            enabled = {6},
            synchronous_commit = {7},
            connect = {8}
        )
        """.format(self._subName,
                    conn.replace("'",''),
                    ','.join(self._publications),
                    self._copy_data,
                    self._create_slot,
                    self._slot_name if self._slot_name == "" else ("slot_name = " + self._slot_name + ","),
                    self._enabled,
                    self._synchronous_commit,
                    self._connect
                    )
        return sql