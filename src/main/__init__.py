"""
lrmanager

Coded by Emanuel Calvo for OnGres Inc.

This tool is a prototype for a Logical Replication Manager.

"""

import yaml
import sys
import os 
import argparse
import getpass

# Custom
from configYaml import *
from servers import *
from publications import *
from subscriptions import *

from utils import *



from termcolor import colored, cprint # https://pypi.org/project/termcolor/ Do not confuse with colored (another lib)
from errors import *

lrmversion = '0.1' 

help_actions="""
Access and configuration actions
================================

gethba
    Gives the hba entries necessary to be added on the endpoints. Only useful on premises instances.
test        
    Test connections
passhash    
    Uses the vault password (-v) and converts your pass in a hash.
generate    
    Generate the SQLs for the YAML structure.


Pre-check your schemas with these actions
=========================================

report
    Gives a full report of publishable tables, information about runtime in destination, 
    sequences information. This action does not need Pub/Sub configuration, as it looks 
    the entire visible tables (for the user). This go through several things among configuration,
    definitions and sequences.

inspect     
    See your current setup and generate YAML. Also reports the status of Pub/Subs.

checktables 
    Check if tables in connections are publishable and recommends if any existent unique indexes
    are eligible as Replica Identities. 

    This checks _all the tables_ in the connection endpoint. If you alter your tables, you need 
    to re-run this as your DDL have changed.

    Eg.:
    ./lrmanager checktables -v ${HOME}/.lrmvault -c ${PWD}/config.yaml --limit=serversource

Create DDL in Subscription actions
==================================

createtables [Not implemented fully, only outputs pgdump]
    Create DDL definition tables in Subscriptions


Pub/Sub Sequences and Identities check actions
==============================================

identities
    See the status of tables with Identities

sequences
    See sequences in Publication tables 

syncsequences [not implemented, only outputs SQL statement]
    Synchronize sequences between Publisher and Subscribers.
    This option requires --commit for applying changes.

    Only 1 pub (--pub) allowed and sub is not mandatory, if no subs passed as arguments,
    print will be the action. If the publication should take a different connection
    than the YAML conf or if it is not declared in it, declare de connection only in the
    YAML and use --conn specifying the connection.

    ./lrmanager syncsequences -v $HOME/lrm_vault \
        -c ${PWD}/src/main/.config.yaml \
        --pub=test --conn=serverLocal

    If sub is not argumented, default action is print. If sub is "all", it will sync
    all the subscription sequences. If a single sub is passed, the sync will be only
    against that sub.

    Options (to be implemented):
      --sync-seq-mode: "tpc"(Sync using two-phase commit. Hard locking), 
                       "jump" (Adds N to destination sequence, default it shuold estimate)
      --sync-seq-jump: N (a value)
    
    Example:  
    ./lrmanager syncsequences -v $HOME/lrm_vault -c $PWD/src/main/.config.yaml 
    --pub=test --conn=serverLocal --sync-seq-mode=print

TODO

deploy      
    Apply changes
    Options (to be implemented):
        - Creating event triggers to avoid schema changes but so subscribers don't brake.
            This forbids completely any alter. You need to disable triggers and do it manually.
        - The other option is to pause subscriptions (enable) that depend on the table to be changed
            , do the alters, alter the publication and resume subscribers.

monitor

setup
    Creates vault and minimal conf
init
    Creates the state.yaml with the current topology.



./lrmanager passhash -v <your_file> 
Copy/paste this hash into your yaml configuration password token, using | as escape.
eg:
    password: |
    $ANSIBLE_VAULT;1.1;AES256.....
"""

def main():
    parser = argparse.ArgumentParser(description='Logical Replication Manager', 
                                    prog="lrmanager",
                                    epilog="Coded by 3manuek - OnGres Inc. 2018",
                                    formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-c', #type=argparse.FileType('r'), 
                        default='./config.yaml',
                        help='YAML file within configuration. Default: config.yaml')
    parser.add_argument('-v',
                        default="",
                        help="""
                        File with the vault password to decrypt config entries.
                        You can either use LRM_VAULT environment variable.
                        """)
    # Actions
    parser.add_argument('action',
                        choices=['deploy',
                                'install',
                                'gethba',
                                'debug',
                                'publications',
                                'subscriptions',
                                'checktables',
                                'createtables',
                                'sequences', 'syncsequences',
                                'test',
                                'generate',
                                'inspect',
                                'identities',
                                'report',
                                'drop',
                                'passhash'], 
                        default='test', 
                        help=help_actions)

    # General or Cross-action Options
    parser.add_argument('-p', 
                        help="passhash password to hash.")
    parser.add_argument('--commit',action='store_true',default=False,
                        help='Applies really default is dry-run') # Applies really default is dry-run
    parser.add_argument('--limit',action='append',default=None,
                        help='Limit the action to these servers/connections') # works like ansible against connection name
    parser.add_argument('--pub',action='append',default=None,
                        help="""
                            Limit the action to these publications.
                            For syncsequences, only 1 pub is allowed. """) # limits by pub
    parser.add_argument('--sub',action='append',default=None,
                        help='Limit the action to these subscritions') # limits by sub
    parser.add_argument('--conn',action='append',default=None,
                        help="""
                        Assigns a different connection from the YAML to a pubname.
                        This is useful if a Publication that you want to query
                        does not exist in the YAML, but connection does (specially
                        if you use the tool only for exploring).
                        """)
    parser.add_argument('--list',action='store_true',default=False,
                        help='Applies to publications, subscriptions')
    # syncseq section                    
    parser.add_argument('--sync-seq-mode',choices=['tpc','jump'],default=None,
                        help='Applies to syncsequences')                        
    parser.add_argument('--sync-seq-jump',type=int,default=0,
                        help='Applies to syncsequences mode jump. Default 1000.')
    parser.add_argument('--sync-seq-action', default="print",
                        choices=['print','sync'],
                        help="""
                        print:  prints SQL to be executed.
                        sync :  updates seq on destiny (USE --commit also!)
                                otherwise it won't update.
                        """)
    # create table section
    parser.add_argument('--ct-action', default="pgdump",
                        choices=['pgdump','custom'],
                        help="""
                        pgdump:  pgdump print.
                        custom:  creates a SQL from catalog
                        """)    

    parser.add_argument('--version', action='version', version='%(prog)s ' + lrmversion)
    parser.add_argument('--debug',action='store_true',default=False)


    try:
        args = parser.parse_args()
    except Exception as e:
        sys.exit(hErrMessage('FAILED_PARSING_ARGS',str(e)))
        
    if args.action == "passhash":
        passhash(args.v, args.p)
        sys.exit(0)

    yamlConf = configYaml.configYaml(args,args.c)

    # import pdb; pdb.set_trace()

    # Because Python does not support switch case , we ended up
    # doing what a switcher that jumps to the action itself
    def switcher(args,yamlConf):
        func = eval(args.action)
        return func(args, yamlConf)
    switcher(args,yamlConf)
### END MAIN

def test(args, yamlConf):
    conns = getConnections(args,yamlConf)
    testConnections(conns, args.debug) 
    sys.exit(0)

def gethba(args,yamlConf):
    res = {} 
  
    servers = getConnDetails(args,yamlConf)
    print("Get Hba for the local host to the remotes")
    if args.debug:
        print('check',args.limit)
    for s in servers:
        print(getHba(servers[s]))

    sys.exit(hErrMessage('OK'))

def debug(args, yamlConf):
    stream = open('test.yaml','w') # useful for verifying yaml integrity
    yaml.dump(yamlConf, stream)
    print(args)
    sys.exit(hErrMessage('OK'))


def install(args,yamlConf):
    print("Not implmented.")
    sys.exit(hErrNum('OK'))


def deploy(args,yamlConf):
    """
    deploy

    WIP

    Implements:
    - createtables on dest subs from pub definition
    - checks the access from dest to sources 
    - creates pub in sources
    - creates sub in dest
    - Warns about seq synchronization and outputs the command for this
    - Implement the concept of a database backend, for storing the state
    """

    conns = getConnections(args,yamlConf)
    pubs = getPubs(args, yamlConf)
    subs = getSubs(args, yamlConf)

    if args.commit:
        print("Changes are going to have effect.")
    else:
        print("Dry-run. Use --commit to apply")




    pass

def syncsequences(args,yamlConf):
    """
    Arguments:
    mode (twophasecommit, lightweight, jump+N)

    If we want consistency in extreme so the hackish way could be
    something like:

    - Pause all subscriptions.
    - Begin tx (on all subscribers) and prepare txs

    # begin transactions for each database connection
    conns[serverName].tpc_begin(conn1.xid(42, 'transaction ID', 'connection 1'))
    conns[serverName].tpc_begin(conn2.xid(42, 'transaction ID', 'connection 2'))
    # Do stuff with both connections
    ...
    try:
        conn1.tpc_prepare()
        conn2.tpc_prepare()
    except DatabaseError:
        conn1.tpc_rollback()
        conn2.tpc_rollback()
    else:
        conn1.tpc_commit()
        conn2.tpc_commit()

    This step could be very latency-prune so the recommended way is 
    to add N values (can be heuristically calculated as a fraction
    of the current value and consider the table "age" for 
    an accurate cherry pick of the jump value).

    """
    res = {}
    jump = False
    # Before any action, we load the publication tables and we 
    # get their sequences values with their respectives FQDN name.
    # We discard oids as there is no dependency between catalogs.
    if args.pub is None:
        sys.exit(hErrMessage('NO_PUBS_SYNCSEQ'))
    if len(args.pub) > 1:
        sys.exit(hErrMessage('TOO_MANY_PUBS_SYNCSEQ'))

    servers = getServers(args,yamlConf)
    subs = getSubs(args, yamlConf)
    pubs = getPubs(args, yamlConf)      # XXX: In the future we only want to instantiate 1.
    action = args.sync_seq_action 
    pubSource = args.pub[0]
    customConn = args.conn[0]
    customConnStr = servers[args.conn[0]]
    util = Util()


    # This checks that if pub is not in YAML, conn needs 
    # to be specified (use an existing connection)
    if pubSource not in pubs and customConn is None:
        sys.exit(hErrMessage('PUB_NOT_EXIST'))

    if customConn not in servers:
        sys.exit(hErrMessage('CONN_NOT_IN_YAML'))

    if args.sub is None:
        action = "print"
    else:
        subDest = args.sub

    if args.conn is not None:
        pubconn = args.conn[0]
        servers[pubconn].execsql(util.getAllPubLastValSeqSQL(pubSource))
        res[pubconn] = servers[pubconn].fetchall()
    else:
        for pubconn in pubs[pubSource].getPubConnections():
            res = {}
            servers[pubconn].execsql(pubs[pubSource].getAllPubLastValSeq())
            res[pubconn] = servers[pubconn].fetchall()

    if args.sync_seq_action == "jump":
        jump = True
        print("Sequences will have added {0} to their values".format(args.sync_seq_jump))

    if action == "print":
        for _conn in pubconn:
            servers[pubconn].execsql(util.getPubSequences(pubSource))
            res = servers[pubconn].fetchall()
        for row in res:
            if row[1] is not None:
                print(util.setSequenceValueSQL(row[0], row[1] if jump is True else row[1] + args.sync_seq_jump ))
    pass

def sequences(args,yamlConf):
    res = {} 
    util = Util()
    servers = getServers(args,yamlConf)

    if args.debug:
        print('check',args.limit)

    for s in servers:
        if args.limit is None or (args.limit is not None and s in args.limit):
            try:
                servers[s].execsql(util.getAllLastValSeq())
                res[s] = servers[s].fetchall()
            except Exception as e:
                sys.exit(hErrMessage('ERR_QUERY_SEQS',e))

            qxprint(s,'connection')
            qxprint("Seq Oid\tLast Val\tSeq Name",'header')
            
            for row in res[s]:
                fRow = "{0}\t{2}\t\t{1}".format(row[0],row[1],row[2] if row[2] is not None else "Not set")
                qxprint(fRow, 'normal')
            # print(util.setSequenceValueSQL(row[0], row[1] if jump is True else row[1] + args.sync_seq_jump ))


def report(args, yamlConf):
    conns = getConnections(args,yamlConf)
    servers = getServers(args,yamlConf)
    resSendSSett = {}
    resSubSett = {} 
    util = Util()


    try:
        qxprint("Inspecting endpoints", 'section')
        inspect(args, yamlConf)

        qxprint("Logical Replication Settings", 'section')
        for s in servers:
            qxprint("{0}".format(s), 'connection')
            if args.limit is None or (args.limit is not None and s in args.limit):
                resSendSSett = {}
                servers[s].execsql(util.getLRSendServSettings())
                resSendSSett[s] = servers[s].fetchall()
                resSubSett = {}
                servers[s].execsql(util.getLRSubSettings())
                resSubSett[s] = servers[s].fetchall()
                qxprint("Sending Server Settings", 'item')
                qxprint(resSendSSett[s], 'normal')            
                qxprint("Subscription Settings",'item')
                qxprint(resSubSett[s], 'normal')
        
        qxprint("Checking potential custom Pub/Sub/RepId: ",'section')
        checktables(args,yamlConf)

        qxprint("Sequences", 'section')
        sequences(args,yamlConf)
    except Exception as e:
        # sys.exit(hErrMessage('FAIL_READ_CONN',str(e))) # XXX in the future this is going to catch properly
        print("end")

def createtables(args,yamlConf):
    """
    This action wraps createTablesInServer


    Iterate over tables, use util.getTableCreation(tablename) or pgdump

    """
    conns = getConnections(args,yamlConf)
    servers = getServers(args,yamlConf)
    subs = getSubs(args, yamlConf)
    pubs = getPubs(args, yamlConf)      # XXX: In the future we only want to instantiate 1.
    util = Util()
    if args.pub is None:
        print(colored("All publications from YAML picked up.", "red"))

    # WIP
    if args.ct_action == "pgdump":
        pass
        pgdumpCommand = """
        pg_dump --no-publications --no-subscriptions --schema-only \
        --clean --no-owner --no-privileges --disable-triggers \
        0  1 | psql 2 3
        """.format() # 0 connstring 1 dbsource 2 connstringdest 3 db dest 
                        # probably not needed as connstring has dbname

    if args.ct_action == "custom":
        pass

    pass


def inspect(args, yamlConf):
    """
    not implemented, this needs to extract endpoint pubs/subs and 
    give a YAML or its SQL
    """
    pubs = {}
    slots = {}
    subs = {}
    subsStatus = {}
    util = Util()
    servers = getServers(args, yamlConf)

    for s in servers:
        if args.debug:
            print(subsStatus)
        if args.limit is None or (args.limit is not None and s in args.limit):
            try:
                servers[s].execsql(util.getDefinedPubs())
                pubs[s] = servers[s].fetchall()
                servers[s].execsql(util.getDefinedSubs())
                subs[s] = servers[s].fetchall()
                servers[s].execsql(util.getSlotsStatus())
                slots[s] = servers[s].fetchall()
            except Exception as e:
                sys.exit(hErrMessage('FAIL_READ_CONN',str(e)))

            qxprint("Slots found in {0}:".format(s), 'item')

            for row in slots[s]:
                qxprint(str(row), 'item')

            qxprint("Publication found in {0}:".format(s), 'item')

            for row in pubs[s]:
                # fRow = "{0}\t{2}\t\t{1}".format(row[0],row[1],row[2] if row[2] is not None else "Not set")
                qxprint(util.genYamlPub(row,s), 'normal')

            qxprint("Subs found in {0}: ".format(s),'item')

            for row in subs[s]:
                # fRow = "{0}\t{2}\t\t{1}".format(row[0],row[1],row[2] if row[2] is not None else "Not set")
                qxprint(util.genYamlSub(row,s), 'normal')
                servers[s].execsql(util.getSubStatus(row[0]))
                subsStatus[s] = servers[s].fetchall()
                qxprint("Subs status in {0}: ".format(s),'item')
                qxprint(subsStatus[s], 'normal')


def checktables(args,yamlConf):
    res = {}
    util = Util()
    servers = getServers(args,yamlConf)
    tags = {'fullpub','insonlypub'} # will group very generally
    # fullPubs = {} 
    # insPubs = {}
    insPotRepId = {} # This contains the potential indexes that can be used for replica identity
   
    for s in servers:
        if args.limit is None or (args.limit is not None and s in args.limit):
            try:
                servers[s].execsql(util.checkAllTables())
                res[s] = servers[s].fetchall()
            except Exception as e:
                sys.exit(hErrMessage('FAIL_READ_CONN',str(e)))
    
    # We collect tables and assign their corresponding 
    # publication
    for conn in res:
        fullPubs = []
        insPubs = []
        # insPotRepId = []

        for row in res[conn]:
            if args.debug:
                print("debug",row)
            if row[1] is True:
                fullPubs.append(row[0])
            else:
                insPubs.append(row[0])
                # insPotRepId.append(row[4])

        qxprint(conn,'connection')

        if not fullPubs and not insPubs:
            print("No tables found in public schema.") # XXX: find a better message
        else:
            qxprint("Potential configuration: ", 'subsection')
            # print(colored("\nPossible custom conf for connection: {0} ".format(conn),"yellow",attrs=['bold']))

        if fullPubs:
            print(util.getCreatePubExplicitTablesSQL('fullpub',fullPubs))
        if insPubs:
            print(util.getCreatePubExplicitTablesSQL('insonlypub',insPubs,'insert'))  

        # l = [util.getSetRepIdSQL(table,insonlyPubTables_potix[table]) for table in insonlyPubTables]
        # [ print(util.getSetRepIdSQL(i,insPotRepId[conn][i])) for i in insPotRepId[conn] ]
        # print(insPotRepId)
        qxprint("Potential fixes for replica identities:", 'subsection')
        for row in res[conn]:
            if row[4] is not None and row[1] == False:
                # There is a potential index
                print(util.getSetRepIdSQL(row[0],row[4]))
            if row[4] is None or row[4] == False:
                print("-- Table {0} does not have any unique indexes.".format(row[0]))

    print("""
    Note: FULL can be used only in very specific situations. eg The table does have low amount
    of columns (FULL does record all the OLD values), it has indexes (preferably on all columns),
    it has a 1:1 row matching and its write rate isn't enough to cause impact through write amplification.
    """)
      

def generate(args, yamlConf):
    """
    generate
    Generate SQL from YAML
    """
    util = Util()
    conns = getConnections(args,yamlConf)
    pubs = getPubDef(args, yamlConf)
    subs = getSubs(args, yamlConf)

    qxprint("Publications",'section')

    for pubName in pubs:
        print("Connections: ", pubs[pubName].getPubConnections())
        print(pubs[pubName].getCreatePubSQL().lstrip())

    qxprint("Subscriptions",'section')
    for subName in subs:
        for subCons in subs[subName].getSubConns():
            print(subs[subName].getSubConns())
            print(subs[subName].getCreateSubSQL(conns[subCons]).lstrip())


def passhash(vaultFile,passToHash):
    util = Util()
    passwd = ""

    if vaultFile is None or vaultFile == "":
        print("You need to define a password vault file.")
        print("Create a file with a password in a secure place.")
        print("Or either export LRM_VAULT")
        sys.exit(1) # use error dictionary
    
    if passToHash is None or passToHash == "":
        passwd = getpass.getpass("Password to be hashed:")
    else:
        passwd = passToHash   

    v = ExtendedVault(getVaultP(vaultFile))

    print(v.dump_raw(passwd).decode('utf-8'))
