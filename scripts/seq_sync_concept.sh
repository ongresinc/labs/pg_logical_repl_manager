#!/bin/bash

PRIMARY_HOST=$1
SUPERUSER_PRIMARY=$2
DATABASE_PRIMARY=$3

DEFAULT_PORT=5432

SECONDARY_HOST=$4

PRIMARY_PORT=${5:-$DEFAULT_PORT}

    optional
    SUPERUSER_SECOND=${6:-$SUPERUSER_PRIMARY}
    DATABASE_SECOND=${7:-$DATABASE_PRIMARY}
    SECONDARY_PORT=${8:-$DEFAULT_PORT}

cat /dev/null > /tmp/alter_sequences.sql

if [ "$1" == "h" ] || [ "$1" == "-help" ]; then
echo " Usage: check_LR_sequences.sh PRIMARY_host SUPERUSER_PRIMARY DATABASE_PRIMARY SECONDARY_host [PRIMARY_PORT] [SUPERUSER_SECOND] [DATABASE_SECOND] [SECONDARY_PORT].
if SUPERUSER_SECOND or DATABASE_SECOND are not present SUPERUSER_PRIMARY and DATABASE_PRIMARY will be used instead "
exit 0;
fi

if [ $# -lt 4 ]; then
echo "Missing parameter. Usage: check_LR_sequences.sh PRIMARY_host SUPERUSER_PRIMARY DATABASE_PRIMARY SECONDARY_host [SUPERUSER_SECOND][DATABASE_SECOND]"
exit 0;
fi

echo "You´ll be asked for the superuser/schema_owner password!!!"

psql -h $PRIMARY_HOST -p $PRIMARY_PORT -U $SUPERUSER_PRIMARY -W -d $DATABASE_PRIMARY -t -c "select 'CREATE SEQUENCE IF NOT EXISTS '||schemaname||'.'||sequencename||';' from pg_sequences " -c " select 'ALTER SEQUENCE '||sequencename||' RESTART WITH ' ||last_value|| ';' from pg_sequences ; " > /tmp/alter_sequences.sql
if [ $? -eq 0 ]; then
psql -h $SECONDARY_HOST -p $SECONDARY_PORT -U $SUPERUSER_SECOND -W -d $DATABASE_SECOND -e -f /tmp/alter_sequences.sql;
if [ $? -eq 0 ]; then
echo "All Sequences in ${SECONDARY_HOST}:${DATABASE_SECOND} has been updated to the last value in ${PRIMARY_HOST}:${DATABASE_PRIMARY} ";
fi
else
echo "Please review ERROR message from psql "

fi

exit 0;
