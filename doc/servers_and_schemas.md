## Server creation and schema import

It is intended to support subcscription/pub viewers that allow to 
view the contents of each.

For creating the FDW environment is necessary to create the bellow
objects:

test=> \h import foreign schema
Command:     IMPORT FOREIGN SCHEMA
Description: import table definitions from a foreign server
Syntax:
IMPORT FOREIGN SCHEMA remote_schema
    [ { LIMIT TO | EXCEPT } ( table_name [, ...] ) ]
    FROM SERVER server_name
    INTO local_schema
    [ OPTIONS ( option 'value' [, ... ] ) ]

    CREATE USER MAPPING [IF NOT EXISTS] FOR { user_name | USER | CURRENT_USER | PUBLIC }
    SERVER server_name
    [ OPTIONS ( option 'value' [ , ... ] ) ]

test=>
test=> \h create server
Command:     CREATE SERVER
Description: define a new foreign server
Syntax:
CREATE SERVER [IF NOT EXISTS] server_name [ TYPE 'server_type' ] [ VERSION 'server_version' ]
    FOREIGN DATA WRAPPER fdw_name
    [ OPTIONS ( option 'value' [, ... ] ) ]



CREATE SERVER IF NOT EXISTS slave3
    FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (host 'IP', dbname 'adatabase',
    sslmode 'require', port '6432');


CREATE USER MAPPING IF NOT EXISTS FOR localUser
SERVER slave3 OPTIONS (user 'remoteUser', password 'passwoooord');

IMPORT FOREIGN SCHEMA public
FROM SERVER slave3
INTO public;