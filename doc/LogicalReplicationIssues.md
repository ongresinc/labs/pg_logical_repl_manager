
## Logical Replication Problems

- The `CREATE PUBLICATION` does not check if there is a replica identity on the relation
  to be added, in consequence updates and deletes may perform bad if this is the case or
  may not work at all.
- `CREATE PUBLICATION` **does** check if the table is a root paritition and prompts error,
  so this behavior is kindly inconsistent. Partitions are manipulated as standar relations,
  ergo they do not check RI neither.
- `GENERATED ALWAYS` identities are compatible in Logical Replication and recommended to avoid
  inconsistencies on destination (id can't change). Recommended for partitioning either. Although,
  if you expect to write on the destination _some day_, keep in mind that the identity in the destination
  is lost. That means 


## Logical Replication Restrictions

[Source](https://www.postgresql.org/docs/10/static/logical-replication-restrictions.html)

Logical replication currently has the following restrictions or missing functionality. 
These might be addressed in future releases.

- The database schema and DDL commands are not replicated. 
  The initial schema can be copied by hand using pg_dump --schema-only. 
  Subsequent schema changes would need to be kept in sync manually. (Note, however, 
  that there is no need for the schemas to be absolutely the same on both sides.) 
  Logical replication is robust when schema definitions change in a live database: 
  When the schema is changed on the publisher and replicated data starts arriving 
  at the subscriber but does not fit into the table schema, replication will error 
  until the schema is updated. In many cases, intermittent errors can be avoided by 
  applying additive schema changes to the subscriber first.

> sql/mute_ddl.sql manages the control of alters after publication has been created.
> [Identities](#Identities) is not tested yet with logical replication. (the new serial)

- Sequence data is not replicated. The data in serial or identity columns backed 
by sequences will of course be replicated as part of the table, but the sequence 
itself would still show the start value on the subscriber. If the subscriber is 
used as a read-only database, then this should typically not be a problem. If, however, 
some kind of switchover or failover to the subscriber database is intended, then the 
sequences would need to be updated to the latest values, either by copying the current 
data from the publisher (perhaps using pg_dump) or by determining a sufficiently high 
value from the tables themselves.

> sql/sequences_check.sql provides functions for getting with are automated sequences of
> a table and the last value on each

```sql
test=# select sequences_from_table('im_ok_normal_table');
--------------------------
 im_ok_normal_table_i_seq

test=# select last_value_from_table('im_ok_normal_table');
-----------------------
                     3
```



- TRUNCATE commands are not replicated. This can, of course, be worked around by 
using DELETE instead. To avoid accidental TRUNCATE invocations, you can revoke 
the TRUNCATE privilege from tables.

> This can be addressed efficiently by creating an event trigger on each table
> that is part of a publication
> sql/mute_ddl.sql has the function for the exception, the *_trigger.sql has
> trigger definition propotype:

```sql
CREATE TRIGGER anti_truncate
  INSTEAD OF TRUNCATE ON <table>
  FOR EACH STATEMENT
  EXECUTE PROCEDURE warning_message();
```


- Large objects (see Chapter 34) are not replicated. There is no workaround for 
that, other than storing data in normal tables.

> Addressed in sql/large_objects_check.sql, oid isnt' supported on LR as the
> identity of the oid is lost on destination server.

- Replication is only possible from base tables to base tables. That is, the 
tables on the publication and on the subscription side must be normal tables, 
not views, materialized views, partition root tables, or foreign tables. 
In the case of partitions, you can therefore replicate a partition hierarchy one-to-one,
but you cannot currently replicate to a differently partitioned setup. Attempts to 
replicate tables other than base tables will result in an error.

> Partitions are a whole new thing and there are some proven issues when adding
> _non-replica-identity_ partitions into the Publication. Than meas:

```
test=# CREATE PUBLICATION p_test5 FOR TABLE im_not_ok_partition_0;
CREATE PUBLICATION
test=# \d+ im_not_ok_partition_0;
                                           Table "public.im_not_ok_partition_0"
  Column  |            Type             | Collation | Nullable |      Default      | Storage  | Stats target | Description
----------+-----------------------------+-----------+----------+-------------------+----------+--------------+-------------
 group_id | character(2)                |           |          |                   | extended |              |
 stamp    | timestamp without time zone |           |          | clock_timestamp() | plain    |              |
Partition of: im_not_ok_root_partition_not_ok_partitions FOR VALUES IN ('P0')
Partition constraint: ((group_id IS NOT NULL) AND (group_id = 'P0'::character(2)))
Publications:
    "p_test5"
```

- CREATE INDEX CONCURRENTLY might be locked as it uses REPEATEABLE READ which hold until txs with snapshot are released. 

## Identities

Identity support is not yet tested, there should no need for updating sequences in 
this case, although it might not be _compatible_ with Logical Replication (?).

https://blog.2ndquadrant.com/postgresql-10-identity-columns/

```
CREATE OR REPLACE FUNCTION upgrade_serial_to_identity(tbl regclass, col name)
RETURNS void
LANGUAGE plpgsql
AS $$
DECLARE
  colnum smallint;
  seqid oid;
  count int;
BEGIN
  -- find column number
  SELECT attnum INTO colnum FROM pg_attribute WHERE attrelid = tbl AND attname = col;
  IF NOT FOUND THEN
    RAISE EXCEPTION 'column does not exist';
  END IF;

  -- find sequence
  SELECT INTO seqid objid
    FROM pg_depend
    WHERE (refclassid, refobjid, refobjsubid) = ('pg_class'::regclass, tbl, colnum)
      AND classid = 'pg_class'::regclass AND objsubid = 0
      AND deptype = 'a';

  GET DIAGNOSTICS count = ROW_COUNT;
  IF count < 1 THEN
    RAISE EXCEPTION 'no linked sequence found';
  ELSIF count > 1 THEN
    RAISE EXCEPTION 'more than one linked sequence found';
  END IF;  

  -- drop the default
  EXECUTE 'ALTER TABLE ' || tbl || ' ALTER COLUMN ' || quote_ident(col) || ' DROP DEFAULT';

  -- change the dependency between column and sequence to internal
  UPDATE pg_depend
    SET deptype = 'i'
    WHERE (classid, objid, objsubid) = ('pg_class'::regclass, seqid, 0)
      AND deptype = 'a';

  -- mark the column as identity column
  UPDATE pg_attribute
    SET attidentity = 'd'
    WHERE attrelid = tbl
      AND attname = col;
END;
$$;
```
