

- [ ] Support Multi-action Publications (and manage them as 1 using suffixes)
- [ ] Merge getPubSequences and getAllPubLastValSeqSQL methods in util, both do the same differently.
- [ ] Implement _real all tables_, that is, create a publication for each schema on the database (for funcional parallelization). Currently, postgres FOR ALL TABLES in pub only publish `public` tables.
- [ ] Implement monitor when copy_data of the publication tables (isolated and aggregated by table, easy to find issues whe copying).
- [ ] Implement viewers of subs and pubs with FDW
- [WIP] Implement DDL restoration
- [ ] Manage to alter system's max_sync_workers_per_subscription and  max_logical_replication_workers to increase parallelism . https://www.postgresql.org/docs/10/static/runtime-config-replication.html
    **max_sync_workers_per_subscription depends on max_logical_replication_workers **
- Implement a set of rules for determining potential _relatively safe_ tables to be set with FULL 
  as replica identity. Not recommended but in extreme cases, which lrmaneger intends to cover.

This form changes the information which is written to the write-ahead log to identify rows which are updated or deleted. This option has no effect except when logical replication is in use. DEFAULT (the default for non-system tables) records the old values of the columns of the primary key, if any. USING INDEX records the old values of the columns covered by the named index, which must be unique, not partial, not deferrable, and include only columns marked NOT NULL. FULL records the old values of all columns in the row. NOTHING records no information about the old row. (This is the default for system tables.) In all cases, no old values are logged unless at least one of the columns that would be logged differs between the old and new versions of the row.

- Implement hba_conf helper:

```
    print("""
    host    replication     {1}
    hostssl replication     {1}
    host    {0}         {1}
    hostssl {0}         {1}
    """.format()) #db, user, localhostip
```