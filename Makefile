

# setup
# python3 -m venv venv && source venv/bin/activate
# pip install --upgrade pip
# pip3 install -r requirements.txt

# start docker
# check latest version
# https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository
# sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# cd docker/ && sudo docker-compose up

# run tests
# docker should be up
# test/test.sh