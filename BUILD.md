## Build

**Code uses Python3, ergo we'll use its own virtualenv instead installing the
custom.** [Details of venv in Python3](https://docs.python.org/3/library/venv.html)

```bash
python3 -m venv ./venv
source venv/bin/activate
pip3  install -r requirements.txt
```


