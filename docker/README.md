## Dokcer

```
psql -h 127.0.0.1 -p15555 -U postgres
```




```
# dbname is the connection string, not the database name
# Using --slot="replica" needs a pre-existintent slot,  does not create
# --no-sync makes things faster

pg_basebackup -h localhost -p 15555 -U postgres  --checkpoint="spread" --label="replica"  --write-recovery-conf --progress -D .data_replica/
docker-compose start postgres_replica

psql -h 127.0.0.1 -p15556 -U postgres -c "select pg_is_in_recovery()"
```
