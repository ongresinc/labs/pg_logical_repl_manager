#!/bin/bash

export DROP="DROP TABLE IF EXISTS tangerine;"
export INZ="insert into tangerine (source,dia,payload) select md5(inet_server_addr()::text || uuid_generate_v4()::text ) ,  clock_timestamp(), random()::text from generate_series(1,10);"
export COUNT="select count(*) from tangerine"
export CREATE="CREATE TABLE tangerine (source text, dia timestamp, payload text, PRIMARY KEY (source) )  ; "
export PUB1="CREATE PUBLICATION test2 FOR TABLE im_not_ok_lo, im_ok_no_serial WITH (publish='insert');"
export PUB2="CREATE PUBLICATION test FOR ALL TABLES;"

# cd docker/
# docker-compose up -d
# cd ../

# sleep 5

function ppf {
    psql -h localhost -p 15555 -U postgres <<EOF
${@}
EOF
}

function ppg {
    psql -h localhost -p 15555 -U postgres < $1
}


ppf "DROP PUBLICATION test;"
ppf "DROP PUBLICATION test2;"

echo "create"
ppg main.sql

ppf $PUB1
ppf $PUB2
for i in $(seq 1 10)
do
    ppg insert.sql
done