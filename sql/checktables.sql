WITH all_tables AS (
    select (schemaname||'.'||tablename)::regclass relid
    from pg_tables 
    where schemaname not in ('information_schema','pg_catalog')
),
potentialRepIdIx AS (
    select
        t.oid::regclass as table,
        i.relname as index_name,
        array_to_string(array_agg(a.attname), ', ') as column_names
    from
        pg_class t,
        pg_class i,
        pg_index ix,
        pg_attribute a
    where
        (ix.indisunique and indexprs is null) and
        t.oid = ix.indrelid
        and i.oid = ix.indexrelid
        and a.attrelid = t.oid
        and a.attnum = ANY(ix.indkey)
        and t.relkind = 'r'
        -- and t.oid::regclass = ''::regclass
    group by
        t.oid,
        i.relname
    order by
        t.oid,
        i.relname
)
SELECT at.relid,
    pg_relation_is_publishable(at.relid) is not false 
        and pg_get_replica_identity_index(relid) is not null all_events_pub, 
    pg_relation_is_publishable(at.relid) is_publishable,
    pg_get_replica_identity_index(relid) replica_idix,
    pri.index_name potential_repid_ix, -- 4
    pri.column_names potential_repid_cols -- 5
FROM all_tables at(relid) 
    left outer join potentialRepIdIx pri 
    ON (pri.table = at.relid::regclass)
-- WHERE 
--     at.relid::regclass = 'password_reminders'::regclass
