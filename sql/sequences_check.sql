

DROP FUNCTION IF EXISTS sequences_from_table(thetable regclass);
CREATE OR REPLACE FUNCTION sequences_from_table(thetable regclass)
RETURNS setof text AS $sequences_from_table$
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable and adsrc::text ~ 'nextval'
$sequences_from_table$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS sequences_from_table(thetable text);
CREATE OR REPLACE FUNCTION sequences_from_table(thetable text)
RETURNS setof text AS $sequences_from_table$
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable::regclass and adsrc::text ~ 'nextval'
$sequences_from_table$
IMMUTABLE LANGUAGE sql;


DROP FUNCTION IF EXISTS last_value_from_table(thetable text);
CREATE OR REPLACE FUNCTION last_value_from_table(thetable text)
RETURNS SETOF bigint AS $last_value_from_table$
select last_value 
FROM pg_sequences 
where sequencename IN (
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable::regclass and adsrc::text ~ 'nextval'
);
$last_value_from_table$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS last_value_from_table(thetable text);
CREATE OR REPLACE FUNCTION last_value_from_table(thetable regclass)
RETURNS SETOF bigint AS $last_value_from_table$
select last_value 
FROM pg_sequences 
where sequencename IN (
select (regexp_matches(adsrc::text, $$nextval\('([a-zA-Z_-|\"]+)'$$ , 'g') )[1]
from pg_attrdef 
where adrelid = thetable and adsrc::text ~ 'nextval'
);
$last_value_from_table$
IMMUTABLE LANGUAGE sql;

