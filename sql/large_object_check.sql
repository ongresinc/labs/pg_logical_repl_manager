-- Pre-check Large Object
-- The problem on checking the large object is that it only works 
-- if there is inserted data in the column. Otherwise, pg_largeobject[_metadata]
-- won't hold any entry.
-- To fix this we can either return FALSE if column is `oid`, although we need
-- to be careful that do not include system oid column if table is created with it. 
-- (oid adn tableoid columns is disabled by default)
-- If this returns false it means the table has an oid column, which is potentially
-- a large object.

-- Check Large Object
-- To avoid false positives in this check, if the above check returns _false_ it is 
-- needed to re-check if there is no entries in pg_largeobject.
-- If the table is empty, we still fall into a hard situation, so we can "warn" the user
-- that there is a potential large_object field. 

-- Although, the existence of oid in published tables is something that we can't support
-- (not only for Large Objects) as the destination has clue the correspondence of those
-- ids, as they are different identities. Adding a check for large objects specifically
-- can be done within:

-- test=# select count(*) =0 FROM (select i from im_not_ok_lo tc join pg_largeobject pl on (tc.f =  pl.loid) limit 10) existenceispain;
--  ?column?
-- ----------
--  f
-- (1 row)

-- test=# select count(*) =0 FROM (select i from im_ok_normal_table tc join pg_largeobject pl on (tc.f =  pl.loid) limit 10) existenceispain;
--  ?column?
-- ----------
--  t
-- (1 row)

-- For test:
-- SELECT pre_large_object_check('im_not_ok_lo_with_oids'::regclass);
-- SELECT pre_large_object_check('im_not_ok_lo_with_oids');
-- SELECT pre_large_object_check('im_ok_no_serial'::regclass);
-- SELECT pre_large_object_check('im_ok_no_serial');


DROP FUNCTION IF EXISTS pre_large_object_check(thetable regclass);
CREATE OR REPLACE FUNCTION pre_large_object_check(thetable regclass)
RETURNS boolean AS $pre_large_object_check$
 select count(*) = 0 
 from pg_attribute 
 where attrelid = thetable
    and attname NOT IN ('oid', 'tableoid') -- excluding system oid
    and atttypid::regtype = 'oid'::regtype ;
$pre_large_object_check$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS pre_large_object_check(thetable text);
CREATE OR REPLACE FUNCTION pre_large_object_check(thetable text)
RETURNS boolean AS $pre_large_object_check$
 select count(*) = 0 
 from pg_attribute 
 where attrelid = thetable::regclass
    and attname NOT IN ('oid', 'tableoid') -- excluding system oid
    and atttypid::regtype = 'oid'::regtype ;
$pre_large_object_check$
IMMUTABLE LANGUAGE sql;


-- DROP FUNCTION IF EXISTS large_object_check(thetable text);
-- CREATE OR REPLACE FUNCTION large_object_check(thetable text)
-- RETURNS boolean AS $large_object_check$
--  select count(*) = 0 
--  from pg_attribute 
--  where attrelid = thetable::regclass
--     and attname NOT IN ('oid', 'tableoid') -- excluding system oid
--     and atttypid::regtype = 'oid'::regtype ;
-- $large_object_check$
-- IMMUTABLE LANGUAGE sql;

-- DROP FUNCTION IF EXISTS large_object_check(thetable text);
-- CREATE OR REPLACE FUNCTION large_object_check(thetable text)
-- RETURNS boolean AS $large_object_check$
--  select count(*) = 0 
--  from pg_attribute 
--  where attrelid = thetable::regclass
--     and attname NOT IN ('oid', 'tableoid') -- excluding system oid
--     and atttypid::regtype = 'oid'::regtype ;
-- $large_object_check$
-- IMMUTABLE LANGUAGE sql;


