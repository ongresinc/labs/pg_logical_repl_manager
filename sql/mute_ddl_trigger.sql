CREATE EVENT TRIGGER anti_ddl_{{table_oid}}
ON ddl_command_start
   EXECUTE PROCEDURE warning_ddl();