DROP FUNCTION IF EXISTS version_check(min_supp_version int);
CREATE OR REPLACE FUNCTION version_check(min_supp_version int) RETURNS boolean 
AS $version_check$
    select (regexp_match(version(),'[PostgreSQL]\s(\d+)'))[1]::int >= min_supp_version 
$version_check$ language sql IMMUTABLE ;

