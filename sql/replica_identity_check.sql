

-- pg_index has indisunique and indisprimary attributes which contains true if this index
-- is elegible for replica identity. So, if a query return no rows means
-- that there is no replica identity
-- https://www.postgresql.org/docs/10/static/sql-altertable.html#SQL-CREATETABLE-REPLICA-IDENTITY

-- USING INDEX records the old values of the columns covered by the named index, which must be unique, 
-- not partial, not deferrable, and include only columns marked NOT NULL. FULL records the old values of 
-- all columns in the row. NOTHING records no information about the old row. (This is the default for system tables.) 
-- In all cases, no old values are logged unless at least one of the columns that would be logged 
-- differs between the old and new versions of the row.

-- indisreplident only is set to true when ALTER TABLE ... REPLICA IDENTITY is being issued.

-- Postgres does not check this
-- test=# CREATE PUBLICATION p_test3 FOR TABLE im_not_ok_no_replica_identity ;
-- CREATE PUBLICATION

-- SELECT replica_identity_check('im_ok_normal_table');
-- SELECT replica_identity_check('im_ok_fancy_auto_generated')

DROP FUNCTION IF EXISTS replica_identity_check(thetable regclass);
CREATE OR REPLACE FUNCTION replica_identity_check(thetable regclass)
RETURNS boolean AS $replica_identity_check$
Select count(*) > 0
FROM pg_attribute
Where attidentity is not null 
    and attidentity != ''
    and attrelid = thetable
$replica_identity_check$
IMMUTABLE LANGUAGE sql;

DROP FUNCTION IF EXISTS replica_identity_check(thetable text);
CREATE OR REPLACE FUNCTION replica_identity_check(thetable text)
RETURNS boolean AS $replica_identity_check$
Select count(*) > 0
FROM pg_attribute
Where attidentity is not null 
    and attidentity != ''
    and attrelid = thetable::regclass
$replica_identity_check$
IMMUTABLE LANGUAGE sql;


