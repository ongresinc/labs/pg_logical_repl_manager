CREATE TRIGGER anti_truncate_{{table_oid}}
  INSTEAD OF TRUNCATE ON {{table_oid::regclass}}
  FOR EACH STATEMENT
  EXECUTE PROCEDURE warning_message();