DROP FUNCTION IF EXISTS LR_validation();
CREATE OR REPLACE FUNCTION LR_validation()
RETURNS text AS $$
DECLARE
    v text;
    db text;
    dbs int;
    lo int;
    noi text;
    nopi text;
    seqs text;
    vws text;
    tps text;
    result text := 'LR is feasible, OK to go!';
BEGIN
    -- PostgreSQL version
    -- Moved to version_check.sql
    v := version();
    RAISE NOTICE '%', v  || E'\n';
    IF v  !~* '^postgresql 1' THEN
        RAISE EXCEPTION 'PostgreSQL version is lower than 10.0, so logical replication is not available';      
    END IF;  

    -- db info
    db := current_database();
    SELECT count(*) INTO dbs FROM pg_database WHERE datistemplate = false;
    RAISE NOTICE 'Current database: %', db;
    RAISE NOTICE 'Number of databases in this instance: %', dbs || E'\n';

    -- Large Objects check
    -- SELECT count(*) into lo from pg_catalog.pg_largeobject;
    -- IF lo <> 0 THEN
    --     RAISE WARNING '% Large Objects in use, so logical replication is not available', lo;
    --     result := 'LR might fail, check the warnings';
    -- ELSE
    --     RAISE NOTICE 'No Large Objects found%', E'\n';
    -- END IF; 
    
    --  tables without indexes
    -- SELECT 
    --     string_agg(quote_ident(schemaname) ||'.'|| quote_ident(tablename), E'\n') INTO noi 
    -- FROM pg_catalog.pg_tables 
    -- WHERE 
    --     schemaname NOT IN ('information_schema', 'pg_catalog') AND
    --     NOT hasindexes;
    -- IF length(noi) <> 0 THEN
    --     RAISE WARNING 'Tables without any index';
    --     RAISE WARNING '------------------------';
    --     RAISE WARNING '%', noi || E'\n';
    --     RAISE WARNING 'Each and every table must be properly indexed to enable logical replication%', E'\n';
    --     result := 'LR might fail, check the warnings';
    -- ELSE
    --     RAISE NOTICE 'All the tables have at least one index%', E'\n';
    -- END IF;

    -- tables with indexes, but not as unique as required
    -- SELECT
    --     string_agg(t.rel, E'\n') INTO nopi
    -- FROM
    --     (
    --         SELECT 
    --             quote_ident(schemaname) ||'.'|| quote_ident(tablename) as rel
    --         FROM 
    --             pg_catalog.pg_tables
    --         WHERE 
    --         schemaname NOT IN ('information_schema', 'pg_catalog') AND
    --         hasindexes
    --     ) t
    -- CROSS JOIN
    --     LATERAL (
    --         SELECT
    --             1
    --         FROM
    --             pg_index
    --         WHERE 
    --             indrelid = t.rel::regclass::oid AND
    --             NOT indisunique AND
    --             NOT indisprimary AND
    --             NOT indisreplident
    --         LIMIT 1
    --     ) ind;
    -- IF length(nopi) <> 0 THEN
    --     RAISE WARNING 'Indexed tables, but without any unique index';
    --     RAISE WARNING '--------------------------------------------';
    --     RAISE WARNING '%', nopi || E'\n';
    --     RAISE WARNING 'Each and every table must be properly indexed to enable logical replication%', E'\n';
    --     result := 'LR might fail, check the warnings';
    -- END IF;

    -- Sequences
    SELECT 
        string_agg(quote_ident(sequence_schema) ||'.'|| quote_ident(sequence_name), E'\n') INTO seqs 
    FROM information_schema.sequences;
    IF length(seqs) <> 0 THEN
        RAISE WARNING 'Existing sequences';
        RAISE WARNING '------------------';
        RAISE WARNING '%', seqs || E'\n';
        RAISE WARNING 'Sequences ar not synced by logical replication,';
        RAISE WARNING 'therefore they need to be replicated using a different approach%',E'\n';
        result := 'LR might fail, check the warnings';
    END IF;

    -- views, materialized views, partition tables, foreign tables
    SELECT 
        string_agg(quote_ident(n.nspname) || '.' ||quote_ident(c.relname) || ' , ' || relkind , E'\n') INTO vws
    FROM   
        pg_class c 
    CROSS JOIN
        LATERAL(
            SELECT
                nspname
            FROM 
                pg_catalog.pg_namespace
            WHERE
                oid = c.relnamespace AND
                nspacl IS NOT NULL AND
                nspname NOT IN ('information_schema', 'pg_catalog')
        ) n
    WHERE  
        c.relkind IN ('m', 'v', 'f', 'p');   
    IF length(vws) <> 0 THEN
        RAISE WARNING 'Not base tables';
        RAISE WARNING '---------------';
        RAISE WARNING '%', vws || E'\n';
        RAISE WARNING 'Only base tables are synced by logical replication,';
        RAISE WARNING 'views (v), materialized views (m), foreign tables (f) and partition tables (p)';
        RAISE WARNING 'are not replicated%',E'\n';
        result := 'LR might fail, check the warnings';
    ELSE
        RAISE NOTICE 'All the tables are base tables';
    END IF;

    -- check TRUNCTE priviledges
    SELECT 
         string_agg(quote_ident(table_schema) ||'.'|| quote_ident(table_name), E'\n') INTO tps
    FROM   
        information_schema.table_privileges
    WHERE
        privilege_type = 'TRUNCATE' AND
        table_schema NOT IN ('information_schema', 'pg_catalog');
    IF length(tps) <> 0 THEN
        RAISE WARNING 'Tables with TRUNCATE privilege';
        RAISE WARNING '------------------------------';
        RAISE WARNING '%', tps || E'\n';
        RAISE WARNING 'TRUNCATE commands are not replicated, DELETE should be used instead';
        RAISE WARNING 'and revoke TRUNCATE privileges to avoid accidental invocations%', E'\n';
        result := 'LR might fail, check the warnings';
    ELSE
        RAISE NOTICE 'No table has TRUNCATE privileges';
    END IF;        

    DROP FUNCTION LR_validation();

    RETURN result;

END;
$$ language plpgsql volatile;

SELECT LR_validation();